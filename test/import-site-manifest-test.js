/* eslint-env mocha */
'use strict'

const ContentCatalog = require('@antora/content-classifier/content-catalog')
const { expect, trapAsyncError } = require('./harness')
const fs = require('node:fs')
const { promises: fsp } = fs
const http = require('node:http')
const importSiteManifest = require('@antora/atlas-extension/import-site-manifest')
const ospath = require('node:path')
const { once } = require('node:events')
const parseResourceRef = require('@antora/content-classifier/util/parse-resource-id')
const proxyContentCatalog = require('#proxy-content-catalog')

const BASE_CACHE_DIR = ospath.join(__dirname, 'fixtures/.cache')
const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const HOSTNAME = 'localhost'

describe('importSiteManifest', () => {
  let playbook, siteAsciiDocConfig, contentCatalog

  beforeEach(() => {
    playbook = { dir: FIXTURES_DIR, runtime: { cacheDir: './.cache/antora' } }
    siteAsciiDocConfig = { attributes: {} }
    contentCatalog = new ContentCatalog()
  })

  const addFilesAndRegisterStartPages = (contentCatalog) => {
    contentCatalog.getComponents().forEach(({ versions: componentVersions }) => {
      componentVersions.forEach((componentVersion) => {
        const { name: component, files = [], startPage } = componentVersion
        for (let file, iter = files.reverse(); (file = iter.pop());) contentCatalog.addFile(file, componentVersion)
        contentCatalog.registerComponentVersionStartPage(component, componentVersion, startPage)
      })
    })
  }

  // Q: should this case set primary-site-url if defined in manifest?
  it('should take no action if site manifest file empty', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './empty-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig)
    expect(contentCatalog.getComponents()).to.be.empty()
  })

  it('should take no action if site manifest file cannot be read', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './no-such-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig)
    expect(contentCatalog.getComponents()).to.be.empty()
  })

  it('should set primary-site-url attribute read from manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './own-site-manifest.json'
    contentCatalog.registerComponentVersion('the-component', '1.0')
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    expect(siteAsciiDocConfig.attributes).to.have.property('primary-site-url', 'https://docs.example.org')
  })

  it('should not set primary-site-url attribute read from manifest if already set', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './own-site-manifest.json'
    siteAsciiDocConfig.attributes['primary-site-url'] = 'https://example.org/docs'
    contentCatalog.registerComponentVersion('the-component', '1.0')
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    expect(siteAsciiDocConfig.attributes).to.have.property('primary-site-url', 'https://example.org/docs')
  })

  it('should import new component versions from manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
    contentCatalog.registerComponentVersion('the-component', '1.0', { title: 'The Component' })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(2)
    const newComponent = contentCatalog.getComponent('new-component')
    expect(newComponent).to.exist()
    expect(newComponent).to.have.nested.property('site.url', 'https://docs.example.org')
    expect(newComponent.title).to.equal('New Component')
    const versions = newComponent.versions
    expect(versions).to.have.lengthOf(2)
    versions.forEach((version) => {
      expect(version.displayVersion).to.equal(version.version)
      expect(version).to.have.nested.property('site.url', 'https://docs.example.org')
      expect(version.title).to.equal('New Component')
      expect(version.url).to.equal(`https://docs.example.org/new-component/${version.version}/index.html`)
    })
    expect(newComponent.latest).to.equal(versions[0])
    expect(newComponent.url).to.equal(versions[0].url)
  })

  it('should import prerelease version from manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-prerelease-site-manifest.json'
    contentCatalog.registerComponentVersion('the-component', '1.0', { title: 'The Component' })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const theComponent = contentCatalog.getComponent('the-component')
    expect(theComponent).to.exist()
    expect(theComponent.title).to.equal('The Component')
    const versions = theComponent.versions
    expect(versions).to.have.lengthOf(2)
    const versionMap = {}
    versions.forEach((version) => {
      expect(version.displayVersion).to.equal(version.version)
      expect(version.title).to.equal('The Component')
      versionMap[version.version] = version
    })
    expect(versionMap['2.0']).to.have.nested.property('site.url', 'https://docs.example.org')
    expect(versionMap['2.0'].prerelease).to.be.true()
    expect(theComponent.latest).to.equal(versionMap['1.0'])
    expect(theComponent.url).to.equal(versionMap['1.0'].url)
  })

  it('should assume ROOT module if not specified in manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './no-module-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const newComponent = contentCatalog.getComponent('new-component')
    expect(newComponent).to.exist()
    const files = newComponent.versions[0].files
    expect(files).to.have.lengthOf(1)
    expect(files[0].src.module).to.equal('ROOT')
  })

  it('should set default page title if not specified in manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './no-page-title-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const newComponent = contentCatalog.getComponent('new-component')
    expect(newComponent).to.exist()
    const files = newComponent.versions[0].files
    expect(files).to.have.lengthOf(1)
    expect(files[0].title).to.equal('Untitled')
    expect(files[0].asciidoc.doctitle).to.equal('Untitled')
  })

  it('should only import component versions from manifest not defined in content catalog', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-version-site-manifest.json'
    contentCatalog.registerComponentVersion('the-component', '2.0', { title: 'The Component', startPage: true })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const theComponent = contentCatalog.getComponent('the-component')
    expect(theComponent).to.not.have.property('site')
    expect(theComponent.title).to.equal('The Component')
    const versions = theComponent.versions
    expect(versions).to.have.lengthOf(2)
    expect(versions[0].displayVersion).to.equal(versions[0].version)
    expect(versions[0]).to.not.have.property('site')
    expect(versions[0].title).to.equal('The Component')
    expect(versions[1].displayVersion).to.equal('Legacy')
    expect(versions[1].title).to.equal('The Component')
    expect(versions[1]).to.have.nested.property('site.url', 'https://docs.example.org')
    expect(versions[1].url).to.equal('https://docs.example.org/the-component/1.0/index.html')
    expect(theComponent.latest).to.equal(versions[0])
    expect(theComponent.url).to.equal(versions[0].url)
  })

  it('should not import component with no versions', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './no-versions-site-manifest.json'
    contentCatalog.registerComponentVersion('the-component', '2.0', { title: 'The Component', startPage: true })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const theComponent = contentCatalog.getComponent('the-component')
    expect(theComponent).to.exist()
  })

  it('should create page stubs for pages in imported component version', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    expect(contentCatalog.getComponents()[0].versions).to.have.lengthOf(2)
    addFilesAndRegisterStartPages(contentCatalog)
    const pages = contentCatalog.getPages()
    expect(pages).to.have.lengthOf(2)
    pages.forEach((page) => {
      expect(page.contents).to.be.null()
      expect(page.stat).to.be.null()
      expect(page).to.have.nested.property('site.url', 'https://docs.example.org')
      expect(page).to.have.nested.property(
        'pub.url',
        `https://docs.example.org/${page.src.component}/${page.src.version}/index.html`
      )
      expect(page).to.have.property('title', 'Index Page')
      expect(page).to.have.nested.property('asciidoc.doctitle', 'Index Page')
      expect(page).to.have.nested.property('asciidoc.xreftext', `Get Started with ${page.src.version}`)
      expect(page.src).to.include({
        component: 'new-component',
        module: 'ROOT',
        relative: 'index.adoc',
        family: 'page',
        basename: 'index.adoc',
        extname: '.adoc',
        stem: 'index',
        mediaType: 'text/asciidoc',
      })
    })
  })

  it('should not qualify url of component, version, or page when primary-site-url is .', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
    siteAsciiDocConfig.attributes['primary-site-url'] = '.'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.getComponents()).to.not.be.empty()
    expect(contentCatalog.getPages()).to.not.be.empty()
    contentCatalog.getComponents().forEach((component) => {
      expect(component.url).to.startWith('/')
      expect(component.latest.url).to.startWith('/')
    })
    contentCatalog.getPages().forEach((page) => expect(page.pub.url).to.startWith('/'))
  })

  it('should not qualify url of component, version, or page when site url matches url in site manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
    playbook.site = { url: 'https://docs.example.org' }
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.getComponents()).to.not.be.empty()
    expect(contentCatalog.getPages()).to.not.be.empty()
    contentCatalog.getComponents().forEach((component) => {
      expect(component.url).to.startWith('/')
      expect(component.latest.url).to.startWith('/')
    })
    contentCatalog.getPages().forEach((page) => expect(page.pub.url).to.startWith('/'))
  })

  it('should qualify url of component, version, and page when site url does not match url in site manifest', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
    playbook.site = { url: 'https://docs-archive.example.org' }
    contentCatalog.registerComponentVersion('the-component', '1.0')
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(2)
    expect(contentCatalog.getComponent('the-component').url).to.startWith('/')
    expect(contentCatalog.getComponent('new-component').url).to.startWith('https://docs.example.org/')
  })

  it('should import alias in same site mode', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './with-alias-site-manifest.json'
    siteAsciiDocConfig.attributes['primary-site-url'] = '.'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog, parseResourceRef)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.findBy({ family: 'alias' })).to.have.lengthOf(1)
    const actual = contentCatalog.getById({
      component: 'the-component',
      version: '1.0',
      module: 'ROOT',
      family: 'alias',
      relative: 'the-alias.adoc',
    })
    expect(actual).to.exist()
    const actualUrl = actual.rel.pub.url
    const expected = contentCatalog.getById({
      component: 'the-component',
      version: '1.0',
      module: 'ROOT',
      family: 'page',
      relative: 'the-page.adoc',
    })
    expect(actualUrl).to.equal(expected.pub.url)
    expect(actual.rel.title).to.equal(expected.title)
  })

  it('should import alias as page in sharded mode', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './with-alias-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.findBy({ family: 'alias' })).to.be.empty()
    const actual = contentCatalog.getById({
      component: 'the-component',
      version: '1.0',
      module: 'ROOT',
      family: 'page',
      relative: 'the-alias.adoc',
    })
    expect(actual).to.exist()
    const actualUrl = actual.pub.url
    expect(actualUrl).to.equal(
      contentCatalog.getById({
        component: 'the-component',
        version: '1.0',
        module: 'ROOT',
        family: 'page',
        relative: 'the-page.adoc',
      }).pub.url
    )
  })

  it('should import start page alias in same site mode', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './start-page-as-alias-site-manifest.json'
    siteAsciiDocConfig.attributes['primary-site-url'] = '.'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog, parseResourceRef)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const componentVersion = contentCatalog.getComponentVersion('the-component', '1.0')
    expect(componentVersion).to.exist()
    expect(componentVersion).to.have.property('startPage', 'ROOT:start-page.adoc')
    expect(componentVersion.files.map((it) => it.path)).to.not.include('modules/ROOT/pages/index.adoc')
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.findBy({ family: 'alias' })).to.have.lengthOf(1)
    const actual = contentCatalog.getById({
      component: 'the-component',
      version: '1.0',
      module: 'ROOT',
      family: 'alias',
      relative: 'index.adoc',
    })
    expect(actual).to.exist()
    const actualUrl = actual.rel.pub.url
    expect(actualUrl).to.equal(
      contentCatalog.getById({
        component: 'the-component',
        version: '1.0',
        module: 'ROOT',
        family: 'page',
        relative: 'start-page.adoc',
      }).pub.url
    )
    expect(contentCatalog.getComponents()[0].url).to.equal(actualUrl)
    expect(contentCatalog.getComponents()[0].versions[0].url).to.equal(actualUrl)
  })

  it('should import start page alias as page in sharded mode', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './start-page-as-alias-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    const componentVersion = contentCatalog.getComponentVersion('the-component', '1.0')
    expect(componentVersion).to.exist()
    expect(componentVersion).to.not.have.property('startPage')
    expect(componentVersion.files.map((it) => it.path)).to.include('modules/ROOT/pages/index.adoc')
    addFilesAndRegisterStartPages(contentCatalog)
    expect(contentCatalog.findBy({ family: 'alias' })).to.be.empty()
    const actual = contentCatalog.getById({
      component: 'the-component',
      version: '1.0',
      module: 'ROOT',
      family: 'page',
      relative: 'index.adoc',
    })
    expect(actual).to.exist()
    const actualUrl = actual.pub.url
    expect(actualUrl).to.equal(
      contentCatalog.getById({
        component: 'the-component',
        version: '1.0',
        module: 'ROOT',
        family: 'page',
        relative: 'start-page.adoc',
      }).pub.url
    )
    expect(contentCatalog.getComponents()[0].url).to.equal(actualUrl)
    expect(contentCatalog.getComponents()[0].versions[0].url).to.equal(actualUrl)
  })

  it('should not flag imported home component version as hybrid if not in content catalog', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './home-site-manifest.json'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    const homeComponent = contentCatalog.getComponent('home')
    expect(homeComponent).to.exist()
    expect(homeComponent.versions).to.have.lengthOf(1)
    expect(homeComponent.versions[0]).to.not.have.property('hybrid')
  })

  it('should flag imported home component version as hybrid if already in content catalog', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './home-site-manifest.json'
    Object.assign(contentCatalog.registerComponentVersion('home', ''), {
      files: [
        {
          path: 'modules/ROOT/pages/index.adoc',
          src: { component: 'home', version: '', module: 'ROOT', family: 'page', relative: 'index.adoc' },
        },
      ],
    })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    const homeComponent = contentCatalog.getComponent('home')
    expect(homeComponent).to.exist()
    expect(homeComponent.versions).to.have.lengthOf(1)
    expect(homeComponent.versions[0]).to.have.property('hybrid', true)
    expect(homeComponent.versions[0].url).to.equal('/home/index.html')
    const homePages = contentCatalog.findBy({ component: 'home', family: 'page' })
    expect(homePages).to.have.lengthOf(4)
    const importedHomePages = homePages.filter((candidate) => candidate.site)
    expect(importedHomePages).to.have.lengthOf(3)
    importedHomePages.forEach((page) => {
      expect(page).to.have.nested.property('site.url', 'https://docs.example.org')
      expect(page.pub.url).to.startWith('https://docs.example.org/')
    })
  })

  it('should be able to import into hybrid component with no existing files', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './home-site-manifest.json'
    contentCatalog.registerComponentVersion('home', 'v2')
    contentCatalog.registerComponentVersion('home', '')
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    const homeComponent = contentCatalog.getComponent('home')
    expect(homeComponent).to.exist()
    expect(homeComponent.versions).to.have.lengthOf(2)
    expect(homeComponent.versions.map(({ version }) => version)).to.have.members(['v2', ''])
    const existingComponentVersion = contentCatalog.getComponentVersion(homeComponent, 'v2')
    expect(existingComponentVersion.url).to.equal('/home/v2/index.html')
    const newComponentVersion = contentCatalog.getComponentVersion(homeComponent, '')
    expect(newComponentVersion.url).to.equal('https://docs.example.org/home/index.html')
    expect(newComponentVersion).to.have.property('hybrid', true)
    const homePages = contentCatalog.findBy({ component: 'home', family: 'page' })
    expect(homePages).to.have.lengthOf(4)
  })

  it('should not register page alias for page imported from primary site', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
    const pageWithAliasId = {
      component: 'local',
      version: '1.0',
      module: 'ROOT',
      family: 'page',
      relative: 'page-with-alias.adoc',
    }
    Object.assign(contentCatalog.registerComponentVersion('local', '1.0'), {
      files: [
        {
          path: 'modules/ROOT/pages/page-with-alias.adoc',
          src: pageWithAliasId,
        },
      ],
    })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    contentCatalog = proxyContentCatalog(contentCatalog, parseResourceRef)
    expect(contentCatalog.getComponents()).to.have.lengthOf(2)
    contentCatalog.registerPageAlias('1.0@new-component::index.adoc', contentCatalog.getById(pageWithAliasId))
    expect(contentCatalog.findBy({ family: 'alias' })).to.be.empty()
  })

  it('should recreate page alias for page in hybrid home component imported from primary site', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './home-site-manifest.json'
    const linksPageId = { component: 'home', version: '', module: 'ROOT', family: 'page', relative: 'links.adoc' }
    Object.assign(contentCatalog.registerComponentVersion('home', ''), {
      files: [{ path: 'modules/ROOT/pages/links.adoc', src: linksPageId }],
    })
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    addFilesAndRegisterStartPages(contentCatalog)
    contentCatalog = proxyContentCatalog(contentCatalog, parseResourceRef)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    expect(contentCatalog.findBy({ family: 'alias' })).to.be.empty()
    const expectedUrl = contentCatalog.getById(linksPageId).url
    expect(contentCatalog.resolvePage('all-links.adoc', linksPageId).url).to.equal(expectedUrl)
  })

  it('should gunzip gzipped manifest file', async () => {
    siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './gzipped-site-manifest.json.gz'
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    expect(contentCatalog.getComponent('new-component')).to.exist()
  })

  it('should read site manifest relative to playbook if primary site URL is . and site URL is empty', async () => {
    siteAsciiDocConfig.attributes['primary-site-url'] = '.'
    const expected = ospath.join(FIXTURES_DIR, 'new-component-site-manifest.json')
    await fsp.cp(expected, ospath.join(FIXTURES_DIR, 'site-manifest.json'))
    await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
    expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    expect(contentCatalog.getComponent('new-component')).to.exist()
    expect(contentCatalog.getComponent('new-component').url).to.startWith('/')
  })

  describe('remote manifest', () => {
    let httpServer
    let httpServerUrl

    before(async () => {
      await fsp.rm(BASE_CACHE_DIR, { force: true, recursive: true })
    })

    beforeEach(async () => {
      httpServer = http.createServer((request, response) => {
        if (request.url === '/hang-up.json') {
          response.destroy()
          return
        }
        fs.readFile(ospath.join(FIXTURES_DIR, request.url), (err, content) => {
          if (err) {
            response.writeHead(404, { 'Content-Type': 'text/html' })
            response.end('<!DOCTYPE html><html><body>Not Found</body></html>', 'utf8')
          } else {
            response.writeHead(200, { 'Content-Type': 'application/zip' })
            response.end(content)
          }
        })
      })

      await once(httpServer.listen(0), 'listening').then(() => {
        httpServerUrl = new URL(`http://${HOSTNAME}:${httpServer.address().port}`).toString().slice(0, -1)
      })
    })

    afterEach(async () => {
      await fsp.rm(BASE_CACHE_DIR, { force: true, recursive: true })
      await fsp.rm(ospath.join(FIXTURES_DIR, 'site-manifest.json'), { force: true })
      await once(httpServer.close() || httpServer, 'close')
    })

    it('should throw error if remote manifest is not found', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = `${httpServerUrl}/no-such-site-manifest.json`
      // FIXME: this needs to get logged at the error level
      let actual
      expect(
        await trapAsyncError(() =>
          importSiteManifest(playbook, siteAsciiDocConfig).catch((err) => {
            throw (actual = err)
          })
        )
      ).to.throw('404')
      expect(actual.name).to.equal('HTTPError')
      expect(actual.package).to.equal('@antora/atlas-extension')
    })

    it('should throw error if server hangs up when downloading remote manifest', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = `${httpServerUrl}/hang-up.json`
      // FIXME: this needs to get logged at the error level
      let actual
      expect(
        await trapAsyncError(() =>
          importSiteManifest(playbook, siteAsciiDocConfig).catch((err) => {
            throw (actual = err)
          })
        )
      ).to.throw('socket hang up')
      expect(actual.code).to.equal('ECONNRESET')
      expect(actual.package).to.equal('@antora/atlas-extension')
    })

    it('should download and cache remote manifest', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = `${httpServerUrl}/empty-site-manifest.json`
      await importSiteManifest(playbook, siteAsciiDocConfig)
      expect(contentCatalog.getComponents()).to.be.empty()
      const actual = ospath.join(playbook.dir, playbook.runtime.cacheDir, `${HOSTNAME}-site-manifest.json`)
      const expected = ospath.join(FIXTURES_DIR, 'empty-site-manifest.json')
      expect(actual).to.be.a.file().and.equal(expected)
    })

    it('should use cached remote manifest if it exists', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = `${httpServerUrl}/empty-site-manifest.json`
      const actual = ospath.join(playbook.dir, playbook.runtime.cacheDir, `${HOSTNAME}-site-manifest.json`)
      const expected = ospath.join(FIXTURES_DIR, 'new-component-site-manifest.json')
      await fsp.cp(expected, actual)
      await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
      expect(contentCatalog.getComponent('new-component')).to.exist()
      expect(actual).to.be.a.file().and.equal(expected)
    })

    it('should redownload remote manifest if fetch flag is true', async () => {
      playbook.runtime.fetch = true
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = `${httpServerUrl}/new-component-site-manifest.json`
      const actual = ospath.join(playbook.dir, playbook.runtime.cacheDir, `${HOSTNAME}-site-manifest.json`)
      const existing = ospath.join(FIXTURES_DIR, 'empty-site-manifest.json')
      const expected = ospath.join(FIXTURES_DIR, 'new-component-site-manifest.json')
      await fsp.cp(existing, actual)
      await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
      expect(contentCatalog.getComponent('new-component')).to.exist()
      expect(actual).to.be.a.file().and.equal(expected)
    })

    it('should download default remote manifest if only primary site URL is specified', async () => {
      siteAsciiDocConfig.attributes['primary-site-url'] = httpServerUrl
      const actual = ospath.join(playbook.dir, playbook.runtime.cacheDir, `${HOSTNAME}-site-manifest.json`)
      const expected = ospath.join(FIXTURES_DIR, 'new-component-site-manifest.json')
      await fsp.cp(expected, ospath.join(FIXTURES_DIR, 'site-manifest.json'))
      await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
      const attrs = siteAsciiDocConfig.attributes
      expect(attrs).to.have.property('primary-site-url', httpServerUrl)
      expect(attrs).to.have.property('primary-site-manifest-url', `${httpServerUrl}/site-manifest.json`)
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
      expect(contentCatalog.getComponent('new-component')).to.exist()
      expect(actual).to.be.a.file().and.equal(expected)
    })

    it('should download default remote manifest from site URL if primary site URL is .', async () => {
      playbook.site = { url: httpServerUrl }
      siteAsciiDocConfig.attributes['primary-site-url'] = '.'
      const actual = ospath.join(playbook.dir, playbook.runtime.cacheDir, `${HOSTNAME}-site-manifest.json`)
      const expected = ospath.join(FIXTURES_DIR, 'new-component-site-manifest.json')
      await fsp.cp(expected, ospath.join(FIXTURES_DIR, 'site-manifest.json'))
      await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
      expect(contentCatalog.getComponent('new-component')).to.exist()
      expect(contentCatalog.getComponent('new-component').url).to.startWith('/')
      expect(actual).to.be.a.file().and.equal(expected)
    })

    it('should gunzip remote gzipped manifest file', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = `${httpServerUrl}/gzipped-site-manifest.json.gz`
      const actual = ospath.join(playbook.dir, playbook.runtime.cacheDir, `${HOSTNAME}-site-manifest.json.gz`)
      const expected = ospath.join(FIXTURES_DIR, 'gzipped-site-manifest.json.gz')
      await importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog)
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
      expect(contentCatalog.getComponent('new-component')).to.exist()
      expect(actual).to.be.a.file().and.equal(expected)
    })
  })
})
