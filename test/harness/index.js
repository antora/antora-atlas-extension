/* eslint-env mocha */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.use(require('chai-fs'))
chai.use(require('chai-spies'))
chai.use(require('./chai/start-with'))
// dirty-chai must be loaded after the other plugins
// see https://github.com/prodatakey/dirty-chai#plugin-assertions
chai.use(require('dirty-chai'))

const ajv = new (require('ajv'))({ allErrors: true, useDefaults: true })
const ajvFormats = require('ajv-formats/dist/formats').fastFormats
for (const format of ['date-time', 'uri', 'uri-reference']) ajv.addFormat(format, ajvFormats[format])

// NOTE async keyword only needed on fn declaration if the function it calls does not always return a Promise
const trapAsyncError = (fn) =>
  fn().then(
    (returnValue) => () => returnValue,
    (err) => () => {
      throw err
    }
  )

module.exports = { ajv, expect: chai.expect, spy: chai.spy, trapAsyncError }
