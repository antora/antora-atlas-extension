/* eslint-env mocha */
'use strict'

const { ajv, expect } = require('./harness')
const fsp = require('node:fs/promises')
const ospath = require('node:path')
const schema = require('@antora/atlas-extension/schema')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')

describe('site manifest schema', () => {
  it('should export schema', () => {
    expect(schema).to.exist()
    expect(schema.constructor).to.equal(Object)
    expect(schema).to.have.property('$schema')
    expect(schema).to.have.property('title', 'Antora Site Manifest')
    expect(schema).to.have.property('type', 'object')
    expect(schema).to.have.nested.property('properties.components.type', 'object')
  })

  it('should be a valid schema', () => {
    const result = ajv.validateSchema(schema)
    const errors = ajv.errors || []
    delete ajv.errors
    expect(errors).to.eql([])
    expect(result).to.be.true()
  })

  it('should validate valid document and report no errors', async () => {
    const data = await fsp.readFile(ospath.join(FIXTURES_DIR, 'valid-site-manifest.json')).then(JSON.parse)
    const validate = ajv.compile(schema)
    const result = validate(data)
    expect(validate.errors || []).to.eql([])
    expect(result).to.be.true()
  })

  it('should validate invalid document and report errors', async () => {
    const data = await fsp.readFile(ospath.join(FIXTURES_DIR, 'invalid-site-manifest.json')).then(JSON.parse)
    const validate = ajv.compile(schema)
    const result = validate(data)
    expect(result).to.be.false()
    const errors = (validate.errors || []).map(({ instancePath, message }) => [instancePath, message].join(' - '))
    expect(errors).to.eql([
      '/generated - must match format "date-time"',
      '/url - must match format "uri"',
      '/components - must NOT have additional properties',
      "/components/acme/versions/3.1 - must have required property 'url'",
      "/components/acme/versions/3.1/pages/0 - must have required property 'path'",
      '/components/acme/versions/3.1/pages/0 - must NOT have additional properties',
      '/components/acme/versions/3.1/pages - must NOT have duplicate items (items ## 1 and 2 are identical)',
    ])
  })
})
