'use strict'

const exportSiteManifest = require('./export-site-manifest')
const importSiteManifest = require('./import-site-manifest')
const proxyContentCatalog = require('./proxy-content-catalog')
const proxyMapSite = require('./proxy-map-site')

module.exports.register = function () {
  this.once('componentsRegistered', ({ playbook, siteAsciiDocConfig, contentCatalog }) => {
    const asciidocAttrs = siteAsciiDocConfig.attributes
    if (!asciidocAttrs['primary-site-manifest-url'] && !asciidocAttrs['primary-site-url']) return
    const parseResourceRef = this.require('@antora/content-classifier/util/parse-resource-id')
    return importSiteManifest(playbook, siteAsciiDocConfig, contentCatalog, parseResourceRef).then((imported) => {
      if (!imported) return
      this.updateVariables({ contentCatalog: proxyContentCatalog(contentCatalog, parseResourceRef) })
      if (imported.url) return
      this.replaceFunctions({ mapSite: proxyMapSite(this.getFunctions(), playbook, contentCatalog) })
    })
  })

  this.once('beforePublish', ({ playbook, siteAsciiDocConfig, contentCatalog, siteCatalog }) => {
    const siteManifestPath = siteAsciiDocConfig.attributes['site-manifest-path']
    siteCatalog.addFile(exportSiteManifest(contentCatalog, playbook.site.url, siteManifestPath))
  })
}
