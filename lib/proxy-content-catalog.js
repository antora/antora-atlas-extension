'use strict'

function proxyContentCatalog (contentCatalog, parseResourceRef) {
  return new Proxy(contentCatalog, {
    get: (target, property) =>
      property === 'registerPageAlias' ? registerPageAlias.bind(target, parseResourceRef) : target[property],
  })
}

function registerPageAlias (parseResourceRef, spec, rel) {
  const src = parseResourceRef(spec, rel.src, 'page', ['page'])
  if (!src) return
  const component = this.getComponent(src.component)
  if (component) {
    const componentVersion = this.getComponentVersion(component, src.version || component.latest.version)
    if (componentVersion) {
      if (componentVersion.site) return
      if (componentVersion.hybrid && this.getById(src)?.site) this.removeFile({ src })
    }
  }
  return this.registerPageAlias(spec, rel)
}

module.exports = proxyContentCatalog
