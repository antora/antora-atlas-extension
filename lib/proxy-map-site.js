'use strict'

function proxyMapSite ({ mapSite }, playbook, contentCatalog) {
  return new Proxy(mapSite, {
    apply: (target, self) => {
      const publishablePages = contentCatalog.getPages(({ pub }) => pub)
      return target.call(self, playbook, publishablePages)
    },
  })
}

module.exports = proxyMapSite
