'use strict'

const fsp = require('node:fs/promises')
const ospath = require('node:path')

const PROJECT_ROOT_DIR = ospath.join(__dirname, '..')
const CHANGELOG_FILE = ospath.join(PROJECT_ROOT_DIR, 'CHANGELOG.adoc')
//const DOCS_CONFIG_FILE = ospath.join(PROJECT_ROOT_DIR, 'docs/antora.yml')
//const PACKAGE_LOCK_FILE = ospath.join(PROJECT_ROOT_DIR, 'package-lock.json')
const VERSION = process.env.npm_package_version

function getCurrentDate () {
  const now = new Date()
  return new Date(now.getTime() - now.getTimezoneOffset() * 60000)
}

/*
function updateDocsConfig () {
  const hyphenIdx = VERSION.indexOf('-')
  const base = ~hyphenIdx ? VERSION.substr(0, hyphenIdx) : VERSION
  const [major, minor, patch] = base.split('.')
  const prerelease = ~hyphenIdx ? VERSION.substr(hyphenIdx + 1) : undefined
  return fsp
    .readFile(DOCS_CONFIG_FILE, 'utf8')
    .then((desc) =>
      fsp.writeFile(
        DOCS_CONFIG_FILE,
        desc
          .replace(/^version: \S+$/m, `version: ${q(major + '.' + minor)}`)
          .replace(/^prerelease: \S+$/m, `prerelease: ${prerelease ? q('.' + patch + '-' + prerelease) : 'false'}`),
        'utf8'
      )
    )
}
*/

function updateChangelog (releaseDate) {
  return fsp.readFile(CHANGELOG_FILE, 'utf8').then((changelog) =>
    fsp.writeFile(
      CHANGELOG_FILE,
      changelog.replace(/^== (?:(Unreleased)|\d.*)$/m, (currentLine, replace) => {
        const newLine = `== ${VERSION} (${releaseDate})`
        return replace ? newLine : [newLine, '_No changes since previous release._', currentLine].join('\n\n')
      })
    )
  )
}

/*
function q (str) {
  return `'${str}'`
}
*/

;(async () => {
  const releaseDate = getCurrentDate().toISOString().split('T')[0]
  //await updateDocsConfig()
  await updateChangelog(releaseDate)
  //await updatePackageLock()
})()
